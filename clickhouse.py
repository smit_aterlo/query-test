from clickhouse_driver import Client
import time
import _thread
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import threading


def runQuery(clientClickhouse, query):
    clientClickhouse.execute(query)


client1 = Client(host='localhost', database='preseem')
client2 = Client(host='localhost', database='preseem')
client3 = Client(host='localhost', database='preseem')
client4 = Client(host='localhost', database='preseem')


# Sanity Check 
q1 = "SELECT COUNT(*) FROM interface_tx_bytes WHERE ts>='2021-01-10 00:00:00' AND ts<'2021-01-11 00:00:00'"
q2 = "SELECT COUNT(*) FROM tcp_latency WHERE ts>='2021-01-10 00:00:00' AND ts<'2021-01-11 00:00:00'"
q3 = "SELECT COUNT(*) FROM tcp_tx_segments WHERE ts>='2021-01-10 00:00:00' AND ts<'2021-01-11 00:00:00'"
q4 = "SELECT COUNT(*) FROM tcp_tx_retransmitted_segments WHERE ts>='2021-01-10 00:00:00' AND ts<'2021-01-11 00:00:00'"

t1 = client1.execute(q1)
t2 = client2.execute(q2)
t3 = client3.execute(q3)
t4 = client4.execute(q4)

print("interface_tx_bytes", t1)
print("tcp_latency", t2)
print("tcp_tx_segments", t3)
print("tcp_tx_retransmitted_segments", t4)


programStartTime = time.time()

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 2) AND company='-Kk17aBiZitiZ5Q87WNh' AND subscriber_identifier='6552')"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 2) AND company='-Kk17aBiZitiZ5Q87WNh'  AND subscriber_identifier='6552')"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 2) AND company='-Kk17aBiZitiZ5Q87WNh'  AND subscriber_identifier='6552')"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 2) AND company='-Kk17aBiZitiZ5Q87WNh'  AND subscriber_identifier='6552')"

startTime = time.time()

try:
    t1 = threading.Thread(target=runQuery, args=(client1, q1))
    t2 = threading.Thread(target=runQuery, args=(client2, q2))
    t3 = threading.Thread(target=runQuery, args=(client3, q3))
    t4 = threading.Thread(target=runQuery, args=(client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)


print("Batch 1 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 4) AND company='-L0QPfc80VVqcF9AMFUT' AND topology2='Wanamingo')"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 4) AND company='-L0QPfc80VVqcF9AMFUT' AND topology2='Wanamingo')"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 4) AND company='-L0QPfc80VVqcF9AMFUT' AND topology2='Wanamingo')"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 4) AND company='-L0QPfc80VVqcF9AMFUT' AND topology2='Wanamingo')"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 2 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 8) AND company='-LYlkqPKb56QnmQ4RKnS' GROUP BY ts)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 8) AND company='-LYlkqPKb56QnmQ4RKnS' GROUP BY ts)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 8) AND company='-LYlkqPKb56QnmQ4RKnS' GROUP BY ts)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 8) AND company='-LYlkqPKb56QnmQ4RKnS' GROUP BY ts)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 3 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 3) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, ip_address)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]),SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 3) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, ip_address)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 3) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, ip_address)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 3) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, ip_address)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 4 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE ts>='2021-01-06 00:00:00' AND ts<'2021-01-06 15:00:00' AND company='-LFbWVqrlSe-yHs1SDFE' AND topology2='ORGRGE112 Orange Grove GE' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]),SUM(\"sum\") FROM tcp_latency WHERE ts>='2021-01-06 00:00:00' AND ts<'2021-01-06 15:00:00' AND company='-LFbWVqrlSe-yHs1SDFE' AND topology2='ORGRGE112 Orange Grove GE' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE ts>='2021-01-06 00:00:00' AND ts<'2021-01-06 15:00:00' AND company='-LFbWVqrlSe-yHs1SDFE' AND topology2='ORGRGE112 Orange Grove GE' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE ts>='2021-01-06 00:00:00' AND ts<'2021-01-06 15:00:00' AND company='-LFbWVqrlSe-yHs1SDFE' AND topology2='ORGRGE112 Orange Grove GE' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 5 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT company) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT company) FROM tcp_latency WHERE  toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT company) FROM tcp_tx_segments WHERE  toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT company) FROM tcp_tx_retransmitted_segments WHERE  toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 6 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE ts>=subtractMinutes(ts, 99))"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE ts>=subtractMinutes(ts, 99))"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE ts>=subtractMinutes(ts, 99))"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE ts>=subtractMinutes(ts, 99))"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
except Exception as e:
    t4.join()
    print("Error!!!!!!!!!!", e)
print("Batch 7 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT subscriber_identifier) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT subscriber_identifier) FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT subscriber_identifier) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT subscriber_identifier) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) GROUP BY ts ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 8 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology2) FROM interface_tx_bytes WHERE ts>='2021-01-06 00:11:11' AND ts<'2021-01-06 23:23:23' GROUP BY ts ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology2) FROM tcp_latency WHERE ts>='2021-01-06 00:11:11' AND ts<'2021-01-06 23:23:23' GROUP BY ts ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology2) FROM tcp_tx_segments WHERE ts>='2021-01-06 00:11:11' AND ts<'2021-01-06 23:23:23' GROUP BY ts ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology2) FROM tcp_tx_retransmitted_segments WHERE ts>='2021-01-06 00:11:11' AND ts<'2021-01-06 23:23:23' GROUP BY ts ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 9 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology1) FROM interface_tx_bytes WHERE ts>='2021-01-06 16:16:16' AND ts<'2021-01-07 11:11:11' GROUP BY ts ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology1) FROM tcp_latency WHERE  ts>='2021-01-06 16:16:16' AND ts<'2021-01-07 11:11:11' GROUP BY ts ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology1) FROM tcp_tx_segments WHERE  ts>='2021-01-06 16:16:16' AND ts<'2021-01-07 11:11:11' GROUP BY ts ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 30 minute), COUNT(DISTINCT topology1) FROM tcp_tx_retransmitted_segments WHERE  ts>='2021-01-06 16:16:16' AND ts<'2021-01-07 11:11:11' GROUP BY ts ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 10 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 12) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 12) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 12) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 12) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (client1, q1))
    t2 = threading.Thread(target = runQuery, args = (client2, q2))
    t3 = threading.Thread(target = runQuery, args = (client3, q3))
    t4 = threading.Thread(target = runQuery, args = (client4, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 11 Executed in ", (time.time())-startTime)

q11 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q12 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q13 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-KzzDvFpkNbSHt0I87_t' AND topology1='AP58KTEGOMNI' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q14 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L-ga2aOUMGKuIhqcjvu' AND topology1='CC3AP4 5GHz 450m N' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"

q21 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]), SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q22 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]),SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q23 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]),SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-KzzDvFpkNbSHt0I87_t' AND topology1='AP58KTEGOMNI' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q24 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(buckets.bucketValue[10000]), SUM(buckets.bucketValue[20000]), SUM(buckets.bucketValue[30000]), SUM(buckets.bucketValue[40000]), SUM(buckets.bucketValue[50000]), SUM(buckets.bucketValue[60000]), SUM(buckets.bucketValue[70000]), SUM(buckets.bucketValue[80000]), SUM(buckets.bucketValue[90000]), SUM(buckets.bucketValue[100000]), SUM(buckets.bucketValue[110000]), SUM(buckets.bucketValue[120000]), SUM(buckets.bucketValue[130000]), SUM(buckets.bucketValue[140000]), SUM(buckets.bucketValue[150000]), SUM(buckets.bucketValue[160000]), SUM(buckets.bucketValue[170000]), SUM(buckets.bucketValue[180000]), SUM(buckets.bucketValue[190000]), SUM(buckets.bucketValue[200000]),SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L-ga2aOUMGKuIhqcjvu' AND topology1='CC3AP4 5GHz 450m N' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"

q31 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q32 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q33 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-KzzDvFpkNbSHt0I87_t' AND topology1='AP58KTEGOMNI' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q34 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L-ga2aOUMGKuIhqcjvu' AND topology1='CC3AP4 5GHz 450m N' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"

q41 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L62IIB-kBmNqNXuf3Ib' AND topology1='C-450M-Prin-East' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q42 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-LREppWrK6_Tlcx7p7-d' AND topology1='Dimmitt OLT01' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q43 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-KzzDvFpkNbSHt0I87_t' AND topology1='AP58KTEGOMNI' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"
q44 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), subscriber_identifier, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='-L-ga2aOUMGKuIhqcjvu' AND topology1='CC3AP4 5GHz 450m N' GROUP BY ts, subscriber_identifier, direction ORDER BY 1)"


startTime = time.time()
try:
    t11 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q11))
    t21 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q21))
    t31 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q31))
    t41 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q41))

    t12 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q12))
    t22 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q22))
    t32 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q32))
    t42 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q42))

    t13 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q13))
    t23 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q23))
    t33 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q33))
    t43 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q43))

    t14 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q14))
    t24 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q24))
    t34 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q34))
    t44 = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q44))
    t11.start()
    t21.start()
    t31.start()
    t41.start()
    t12.start()
    t22.start()
    t32.start()
    t42.start()
    t13.start()
    t23.start()
    t33.start()
    t43.start()
    t14.start()
    t24.start()
    t34.start()
    t44.start()
    t11.join()
    t21.join()
    t31.join()
    t41.join()
    t12.join()
    t22.join()
    t32.join()
    t42.join()
    t13.join()
    t23.join()
    t33.join()
    t43.join()
    t14.join()
    t24.join()
    t34.join()
    t44.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 12 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, direction, SUM(gauge) FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' GROUP BY ts, ip_address, direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, direction, SUM(\"sum\") FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' GROUP BY ts, ip_address, direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, direction, SUM(gauge) FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' GROUP BY ts, ip_address, direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT toStartOfInterval(ts, INTERVAL 10 minute), ip_address, direction, SUM(gauge) FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' GROUP BY ts, ip_address, direction ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 1024)

startTime = time.time()
try:
    for index, row in df.iterrows():

        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q1.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q2.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q3.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q4.format(row[0], row[1], row[2])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 13 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' AND topology1='{}' ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 512)

startTime = time.time()
try:
    for index, row in df.iterrows():
        print(index)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q1.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q2.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q3.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q4.format(row[0], row[1], row[2])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

        
except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 14 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE toDateTime(ts)>=subtractHours(toDateTime(now()), 24) AND company='{}' AND topology2='{}' ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 512)

startTime = time.time()
try:
    for index, row in df.iterrows():
        print(index)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q1.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q2.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q3.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (Client(host='localhost', database='preseem'), q4.format(row[0], row[1])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

        
except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 15 Executed in ", (time.time())-startTime)

print("Total Time ", (time.time() - programStartTime))