from pydruid.db import connect
import time
import _thread
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import threading

def runQuery(cursour, query):
    cursour.execute(query)

def runQuery1(cursour, query):
    cursour.execute(query)

conn = connect(host='localhost', port=4545,
               path='/druid/v2/sql/', scheme='http')
curs = conn.cursor()

# Sanity Check 
q1 = "SELECT COUNT(*) FROM interface_tx_bytes WHERE __time>='2021-01-10 00:00:00' AND __time<'2021-01-11 00:00:00'"
q2 = "SELECT COUNT(*) FROM tcp_latency WHERE  __time>='2021-01-10 00:00:00' AND __time<'2021-01-11 00:00:00'"
q3 = "SELECT COUNT(*) FROM tcp_tx_segments WHERE  __time>='2021-01-10 00:00:00' AND __time<'2021-01-11 00:00:00'"
q4 = "SELECT COUNT(*) FROM tcp_tx_retransmitted_segments WHERE  __time>='2021-01-10 00:00:00' AND __time<'2021-01-11 00:00:00'"

t1 = curs.execute(q1)
print("interface_tx_bytes", t1.fetchone())

t2 = curs.execute(q2)
print("tcp_latency", t2.fetchone())

t3 = curs.execute(q3)
print("tcp_tx_segments", t3.fetchone())

t4 = curs.execute(q4)
print("tcp_tx_retransmitted_segments", t4.fetchone())

programStartTime = time.time()

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '2' HOUR AND Company='-Kk17aBiZitiZ5Q87WNh' AND SubscriberIdentifier='6552')"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '2' HOUR AND Company='-Kk17aBiZitiZ5Q87WNh'  AND SubscriberIdentifier='6552')"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '2' HOUR AND Company='-Kk17aBiZitiZ5Q87WNh'  AND SubscriberIdentifier='6552')"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '2' HOUR AND Company='-Kk17aBiZitiZ5Q87WNh'  AND SubscriberIdentifier='6552')"

startTime = time.time()

try:
    t1 = threading.Thread(target=runQuery, args=(curs, q1))
    t2 = threading.Thread(target=runQuery, args=(curs, q2))
    t3 = threading.Thread(target=runQuery, args=(curs, q3))
    t4 = threading.Thread(target=runQuery, args=(curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)


print("Batch 1 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '4' HOUR  AND Company='-L0QPfc80VVqcF9AMFUT' AND Topology2='Wanamingo')"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '4' HOUR  AND Company='-L0QPfc80VVqcF9AMFUT' AND Topology2='Wanamingo')"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '4' HOUR  AND Company='-L0QPfc80VVqcF9AMFUT' AND Topology2='Wanamingo')"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '4' HOUR  AND Company='-L0QPfc80VVqcF9AMFUT' AND Topology2='Wanamingo')"

startTime = time.time()
try:
    t1 = threading.Thread(target=runQuery, args=(curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 2 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '8' HOUR  AND Company='-LYlkqPKb56QnmQ4RKnS' GROUP BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '8' HOUR  AND Company='-LYlkqPKb56QnmQ4RKnS' GROUP BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '8' HOUR  AND Company='-LYlkqPKb56QnmQ4RKnS' GROUP BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '8' HOUR  AND Company='-LYlkqPKb56QnmQ4RKnS' GROUP BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 3 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '3' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, IpAddress)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"),SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '3' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, IpAddress)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '3' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, IpAddress)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '3' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, IpAddress)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 4 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time>='2021-01-06 00:00:00' AND __time<'2021-01-06 15:00:00' AND Company='-LFbWVqrlSe-yHs1SDFE' AND Topology2='ORGRGE112 Orange Grove GE' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"),SUM(\"Sum\") FROM tcp_latency WHERE __time>='2021-01-06 00:00:00' AND __time<'2021-01-06 15:00:00' AND Company='-LFbWVqrlSe-yHs1SDFE' AND Topology2='ORGRGE112 Orange Grove GE' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time>='2021-01-06 00:00:00' AND __time<'2021-01-06 15:00:00' AND Company='-LFbWVqrlSe-yHs1SDFE' AND Topology2='ORGRGE112 Orange Grove GE' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time>='2021-01-06 00:00:00' AND __time<'2021-01-06 15:00:00' AND Company='-LFbWVqrlSe-yHs1SDFE' AND Topology2='ORGRGE112 Orange Grove GE' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 5 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Company) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Company) FROM tcp_latency WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Company) FROM tcp_tx_segments WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Company) FROM tcp_tx_retransmitted_segments WHERE  __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 6 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '99' MINUTE )"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '99' MINUTE )"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '99' MINUTE )"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '99' MINUTE )"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 7 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT SubscriberIdentifier) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT SubscriberIdentifier) FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT SubscriberIdentifier) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT SubscriberIdentifier) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  GROUP BY 1 ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 8 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology2) FROM interface_tx_bytes WHERE __time>='2021-01-06 00:11:11' AND __time<'2021-01-06 23:23:23' GROUP BY 1 ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology2) FROM tcp_latency WHERE __time>='2021-01-06 00:11:11' AND __time<'2021-01-06 23:23:23' GROUP BY 1 ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology2) FROM tcp_tx_segments WHERE __time>='2021-01-06 00:11:11' AND __time<'2021-01-06 23:23:23' GROUP BY 1 ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology2) FROM tcp_tx_retransmitted_segments WHERE __time>='2021-01-06 00:11:11' AND __time<'2021-01-06 23:23:23' GROUP BY 1 ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 9 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology1) FROM interface_tx_bytes WHERE __time>='2021-01-06 16:16:16' AND __time<'2021-01-07 11:11:11' GROUP BY 1 ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology1) FROM tcp_latency WHERE  __time>='2021-01-06 16:16:16' AND __time<'2021-01-07 11:11:11' GROUP BY 1 ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology1) FROM tcp_tx_segments WHERE  __time>='2021-01-06 16:16:16' AND __time<'2021-01-07 11:11:11' GROUP BY 1 ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT30M'), COUNT(DISTINCT Topology1) FROM tcp_tx_retransmitted_segments WHERE  __time>='2021-01-06 16:16:16' AND __time<'2021-01-07 11:11:11' GROUP BY 1 ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 10 Executed in ", (time.time())-startTime)

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '12' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '12' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '12' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '12' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"

startTime = time.time()
try:
    t1 = threading.Thread(target = runQuery, args = (curs, q1))
    t2 = threading.Thread(target = runQuery, args = (curs, q2))
    t3 = threading.Thread(target = runQuery, args = (curs, q3))
    t4 = threading.Thread(target = runQuery, args = (curs, q4))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 11 Executed in ", (time.time())-startTime)




q11 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q12 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q13 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-KzzDvFpkNbSHt0I87_t' AND Topology1='AP58KTEGOMNI' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q14 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L-ga2aOUMGKuIhqcjvu' AND Topology1='CC3AP4 5GHz 450m N' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"

q21 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"), SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q22 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"),SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q23 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"),SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-KzzDvFpkNbSHt0I87_t' AND Topology1='AP58KTEGOMNI' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q24 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(\"Bucket0\"),SUM(\"Bucket1\"),SUM(\"Bucket2\"),SUM(\"Bucket3\"),SUM(\"Bucket4\"),SUM(\"Bucket5\"),SUM(\"Bucket6\"),SUM(\"Bucket7\"),SUM(\"Bucket8\"),SUM(\"Bucket9\"),SUM(\"Bucket10\"),SUM(\"Bucket11\"),SUM(\"Bucket12\"),SUM(\"Bucket13\"),SUM(\"Bucket14\"),SUM(\"Bucket15\"),SUM(\"Bucket16\"),SUM(\"Bucket17\"),SUM(\"Bucket18\"),SUM(\"Bucket19\"),SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L-ga2aOUMGKuIhqcjvu' AND Topology1='CC3AP4 5GHz 450m N' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"

q31 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q32 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q33 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-KzzDvFpkNbSHt0I87_t' AND Topology1='AP58KTEGOMNI' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q34 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L-ga2aOUMGKuIhqcjvu' AND Topology1='CC3AP4 5GHz 450m N' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"

q41 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L62IIB-kBmNqNXuf3Ib' AND Topology1='C-450M-Prin-East' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q42 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-LREppWrK6_Tlcx7p7-d' AND Topology1='Dimmitt OLT01' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q43 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-KzzDvFpkNbSHt0I87_t' AND Topology1='AP58KTEGOMNI' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"
q44 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), SubscriberIdentifier, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='-L-ga2aOUMGKuIhqcjvu' AND Topology1='CC3AP4 5GHz 450m N' GROUP BY 1, SubscriberIdentifier, Direction ORDER BY 1)"


startTime = time.time()
try:
    t11 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q11))
    t21 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q21))
    t31 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q31))
    t41 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q41))
    t12 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q12))
    t22 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q22))
    t32 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q32))
    t42 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q42))
    t13 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q13))
    t23 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q23))
    t33 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q33))
    t43 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q43))
    t14 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q14))
    t24 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q24))
    t34 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q34))
    t44 = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q44))
    t11.start()
    t21.start()
    t31.start()
    t41.start()
    t12.start()
    t22.start()
    t32.start()
    t42.start()
    t13.start()
    t23.start()
    t33.start()
    t43.start()
    t14.start()
    t24.start()
    t34.start()
    t44.start()
    t11.join()
    t21.join()
    t31.join()
    t41.join()
    t12.join()
    t22.join()
    t32.join()
    t42.join()
    t13.join()
    t23.join()
    t33.join()
    t43.join()
    t14.join()
    t24.join()
    t34.join()
    t44.join()
except Exception as e:
    print("Error!!!!!!!!!!", e)
print("Batch 12 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, Direction, SUM(Gauge) FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' GROUP BY 1, IpAddress, Direction ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, Direction, SUM(\"Sum\") FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' GROUP BY 1, IpAddress, Direction ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, Direction, SUM(Gauge) FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' GROUP BY 1, IpAddress, Direction ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT TIME_FLOOR(__time, 'PT10M'), IpAddress, Direction, SUM(Gauge) FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' GROUP BY 1, IpAddress, Direction ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 1024)

startTime = time.time()
try:
    for index, row in df.iterrows():
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q1.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q2.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q3.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q4.format(row[0], row[1], row[2])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 13 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}' AND Topology1='{}' ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 512)

startTime = time.time()
try:
    for index, row in df.iterrows():
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q1.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q2.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q3.format(row[0], row[1], row[2])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q4.format(row[0], row[1], row[2])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 14 Executed in ", (time.time())-startTime)

df = pd.read_csv('topologies.csv')
jobs = []

q1 = "SELECT COUNT(*) FROM (SELECT * FROM interface_tx_bytes WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}'  ORDER BY 1)"
q2 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_latency WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}'  ORDER BY 1)"
q3 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}'  ORDER BY 1)"
q4 = "SELECT COUNT(*) FROM (SELECT * FROM tcp_tx_retransmitted_segments WHERE __time > CURRENT_TIMESTAMP - INTERVAL '24' HOUR  AND Company='{}' AND Topology2='{}'  ORDER BY 1)"

executor = ThreadPoolExecutor(max_workers = 512)

startTime = time.time()
try:
    for index, row in df.iterrows():
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q1.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q2.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q3.format(row[0], row[1])))
        jobs.append(t)
        t = threading.Thread(target = runQuery, args = (connect(host='localhost', port=4545, path='/druid/v2/sql/', scheme='http').cursor(), q4.format(row[0], row[1])))
        jobs.append(t)

    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

except Exception as e:
    print("Error!!!!!!!!!!", e)

print("Batch 15 Executed in ", (time.time())-startTime)

print("Total Time ", (time.time() - programStartTime))